package com.xupt.dangjian;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.xupt.dangjian.model.PageModel;

import com.xupt.dangjian.service.ArticleService;
import com.xupt.dangjian.util.FileUtil;
import com.xupt.dangjian.util.StringUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.expression.EvaluationContext;
import org.springframework.expression.Expression;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.Serializable;
import java.util.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DangjianApplicationTests {
	@Autowired
	FileUtil fileUtil1;

	@Autowired
	FileUtil fileUtil2;

	@Autowired
	ArticleService articleService;

	@Autowired
	RedisTemplate redisTemplate;

	@Test
	public void contextLoads() {
	}

	@Test
	public void yuTest()
	{
       System.out.println(fileUtil1==fileUtil2);

	}

	@Test
	public void regexTest() throws Exception
	{
		System.out.println(fileUtil1.matchStringFromFile("/home/huwei/Desktop/hello.html","\\d{3}"  ));
	}

	@Test
	public void getArticleTest() throws Exception
	{
		PageModel pageModel=new PageModel();
		pageModel.setPageNum( 1 );
		pageModel.setPageSize( 2 );

	}

	@Test
	public void redisTest()
	{
		ValueOperations<Serializable, Object> operations = redisTemplate.opsForValue();
		operations.set("hello", "world");
	}


	@Test
	public void testMap() throws Exception
	{
		Map map=new HashMap();
		map.put( "a","hello" );
		System.out.println(new ObjectMapper(  ).writeValueAsString( map ));
	}

	@Test
	public  void testSpEL()
	{
		ExpressionParser parser = new SpelExpressionParser();
		// 最简单的字符串表达式
		Expression exp = parser.parseExpression("'HelloWorld'");
		System.out.println("'HelloWorld'的结果： " + exp.getValue());
		// 调用方法的表达式
		exp = parser.parseExpression("'HelloWorld'.concat('!')");
		System.out.println("'HelloWorld'.concat('!')的结果： " + exp.getValue());
		// 调用对象的getter方法
		exp = parser.parseExpression("'HelloWorld'.bytes");
		System.out.println("'HelloWorld'.bytes的结果： " + exp.getValue());
		// 访问对象的属性(d相当于HelloWorld.getBytes().length)
		exp = parser.parseExpression("'HelloWorld'.bytes.length");
		System.out.println("'HelloWorld'.bytes.length的结果：" + exp.getValue());
		// 使用构造器来创建对象
		exp = parser.parseExpression("new String('helloworld')" + ".toUpperCase()");
		System.out.println("new String('helloworld')" + ".toUpperCase()的结果是： " + exp.getValue(String.class));


	}


	@Test
	public void stringUtilTest()
	{
		String urlRegex="http://.*((jpg)|(bmp)|(jpg)|(png)|(tif)|(gif)|(pcx)|(tga)|(exif)|(fpx)|(svg)|(psd)|(cdr)|(pcd)|(dxf)|(ufo)|(eps)|(ai)|(raw)|(WMF)|(webp))" ;
		String content="http://39.96.23.138:8008/group1/M00/00/00/rBAOs1ysmrSARnCyAAAuGsujOXw361.png";
		List<String> urlList=StringUtil.matchAll( urlRegex,content );
		for(String str:urlList)
		{
			System.out.println(str);
		}

	}

}
