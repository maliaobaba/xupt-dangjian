package com.xupt.dangjian;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.xupt.dangjian.entity.Article;
import com.xupt.dangjian.service.ArticleService;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ArticleServiceTest {

    @Autowired
    ArticleService articleService;

    @Test
    public void testAddArticle() throws Exception{
        Article article=new Article();
        article.setArticleClass("时讯");
        article.setAuthor("胡伟");
        article.setContent("今天是阳光明媚的一天");
        article.setPublisherId(1);
        article.setCoverImageId(5);
        article.setDangzhanId(1);
        article.setTitle("今天");
        article.setBreviary("阳光明媚");
        articleService.addArticle(article);
    }

    @Test
    public void testGetArticles() throws IOException {
    }
}
