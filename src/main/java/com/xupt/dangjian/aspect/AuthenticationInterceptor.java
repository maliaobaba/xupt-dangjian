package com.xupt.dangjian.aspect;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.fasterxml.jackson.databind.ObjectMapper;

import com.xupt.dangjian.entity.User;
import com.xupt.dangjian.model.NeedRole;
import com.xupt.dangjian.model.PassToken;
import com.xupt.dangjian.model.ResponseModel;
import com.xupt.dangjian.model.UserLoginToken;
import com.xupt.dangjian.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Method;
import java.util.List;

public class AuthenticationInterceptor implements HandlerInterceptor {
   @Autowired
   UserService userService;

    @Override
    public boolean preHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object object) throws Exception {
        String token = httpServletRequest.getHeader( "token" );// 从 http 请求头中取出 token
        // 如果不是映射到方法直接通过
        if (!(object instanceof HandlerMethod)) {
            return true;
        }
        HandlerMethod handlerMethod = (HandlerMethod) object;
        Method method = handlerMethod.getMethod();
        //检查是否有passtoken注释，有则跳过认证
        if (method.isAnnotationPresent( PassToken.class )) {
            PassToken passToken = method.getAnnotation( PassToken.class );
            if (passToken.required()) {
                return true;
            }
        }
        //检查有没有需要用户权限的注解
        if (method.isAnnotationPresent( UserLoginToken.class )) {
            UserLoginToken userLoginToken = method.getAnnotation( UserLoginToken.class );
            if (userLoginToken.required()) {
                // 执行认证
                if (token == null) {

                  setErrorResponse( "没有token，请重新登录",httpServletResponse );
                    return false;

                }
                // 获取 token 中的 user id
                String userId="";
                try {
                    userId = JWT.decode( token ).getAudience().get( 0 );

                } catch (JWTDecodeException j) {

                    setErrorResponse( "token无法解析，请重新登录",httpServletResponse );
                    return false;

                }
                User user = userService.getUserById( Integer.valueOf( userId ) );

                if (user == null) {

                    setErrorResponse( "用户不存在，请重新登录",httpServletResponse );
                    return false;
                }
                // 验证 token
                JWTVerifier jwtVerifier = JWT.require( Algorithm.HMAC256( user.getPassword() ) ).build();
                try {
                    jwtVerifier.verify( token );
                    if (method.isAnnotationPresent( NeedRole.class )) {
                        NeedRole needRole = method.getAnnotation( NeedRole.class );
                        List<String> roles=userService.getRolesById(Integer.valueOf( userId ));
                        String roleStr=needRole.role();
                        for(String role:roles)
                        {
                            if(role.equals( roleStr ))
                            {return true;}
                        }
                        setErrorResponse( "当前用户无权限，执行操作需要为"+roleStr,httpServletResponse );
                        return false;

                    }
                } catch (JWTVerificationException e) {

                    setErrorResponse( "token非法，请重新登录",httpServletResponse );
                   return false;
                }
                return true;
            }
        }
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest httpServletRequest,
                           HttpServletResponse httpServletResponse,
                           Object o, ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest httpServletRequest,
                                HttpServletResponse httpServletResponse,
                                Object o, Exception e) throws Exception {
    }

    public void setErrorResponse(String message,HttpServletResponse httpServletResponse) throws Exception
    {
        ResponseModel responseModel=new ResponseModel();
        responseModel.setStatus( 0 );
        responseModel.setMessage( message);
        httpServletResponse.setHeader("Content-type", "application/json;charset=UTF-8");
        httpServletResponse.getWriter().write(new ObjectMapper().writeValueAsString( responseModel ));
    }
}
