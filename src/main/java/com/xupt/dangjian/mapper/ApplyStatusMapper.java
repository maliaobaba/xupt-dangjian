package com.xupt.dangjian.mapper;

import com.xupt.dangjian.entity.ApplyStatus;
import com.xupt.dangjian.entity.ApplyStatusExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ApplyStatusMapper {
    int countByExample(ApplyStatusExample example);

    int deleteByExample(ApplyStatusExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(ApplyStatus record);

    int insertSelective(ApplyStatus record);

    List<ApplyStatus> selectByExample(ApplyStatusExample example);

    ApplyStatus selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") ApplyStatus record, @Param("example") ApplyStatusExample example);

    int updateByExample(@Param("record") ApplyStatus record, @Param("example") ApplyStatusExample example);

    int updateByPrimaryKeySelective(ApplyStatus record);

    int updateByPrimaryKey(ApplyStatus record);
}