package com.xupt.dangjian.mapper;

import com.xupt.dangjian.entity.StatusRecord;
import com.xupt.dangjian.entity.StatusRecordExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface StatusRecordMapper {
    int countByExample(StatusRecordExample example);

    int deleteByExample(StatusRecordExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(StatusRecord record);

    int insertSelective(StatusRecord record);

    List<StatusRecord> selectByExample(StatusRecordExample example);

    StatusRecord selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") StatusRecord record, @Param("example") StatusRecordExample example);

    int updateByExample(@Param("record") StatusRecord record, @Param("example") StatusRecordExample example);

    int updateByPrimaryKeySelective(StatusRecord record);

    int updateByPrimaryKey(StatusRecord record);
}