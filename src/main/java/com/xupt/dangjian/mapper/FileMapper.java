package com.xupt.dangjian.mapper;

import com.xupt.dangjian.entity.File;
import com.xupt.dangjian.entity.FileExample;
import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface FileMapper {
    int countByExample(FileExample example);

    int deleteByExample(FileExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(File record);

    int insertSelective(File record);

    List<File> selectByExample(FileExample example);

    File selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") File record, @Param("example") FileExample example);

    int updateByExample(@Param("record") File record, @Param("example") FileExample example);

    int updateByPrimaryKeySelective(File record);

    int updateByPrimaryKey(File record);

    @Options(useGeneratedKeys = true,keyProperty = "id",keyColumn = "id")
    @Insert({
            "<script>",
            "insert into file (url,name,size,createdTime,type) values",
            "<foreach collection='fileList' item='item' index='index' separator=','>",
            "(#{item.url}, #{item.name}, #{item.size},#{item.createdTime},#{item.type})",
            "</foreach>",
            "</script>"
    })
    void addFiles(@Param(value="fileList") List<File> fileList);
}