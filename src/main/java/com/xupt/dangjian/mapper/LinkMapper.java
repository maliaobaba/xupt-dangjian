package com.xupt.dangjian.mapper;

import com.xupt.dangjian.entity.Link;
import com.xupt.dangjian.entity.LinkExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

@Repository
public interface LinkMapper {
    int countByExample(LinkExample example);

    int deleteByExample(LinkExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Link record);

    int insertSelective(Link record);

    List<Link> selectByExample(LinkExample example);

    Link selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Link record, @Param("example") LinkExample example);

    int updateByExample(@Param("record") Link record, @Param("example") LinkExample example);

    int updateByPrimaryKeySelective(Link record);

    int updateByPrimaryKey(Link record);

    @Select( "select * from link order by id desc" )
    List<Link> selectAllDesc();

    @Select( "select * from link where linkClass = #{linkClass} order by id desc" )
    List<Link> selectByClass(@Param( "linkClass" ) String linkClass);
}