package com.xupt.dangjian.mapper;

import com.xupt.dangjian.entity.ApplyRecord;
import com.xupt.dangjian.entity.ApplyRecordExample;

import java.util.HashMap;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

@Repository
public interface ApplyRecordMapper {
    int countByExample(ApplyRecordExample example);

    int deleteByExample(ApplyRecordExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(ApplyRecord record);

    int insertSelective(ApplyRecord record);

    List<ApplyRecord> selectByExample(ApplyRecordExample example);

    ApplyRecord selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") ApplyRecord record, @Param("example") ApplyRecordExample example);

    int updateByExample(@Param("record") ApplyRecord record, @Param("example") ApplyRecordExample example);

    int updateByPrimaryKeySelective(ApplyRecord record);

    int updateByPrimaryKey(ApplyRecord record);

    @Select("<script>"
            + "SELECT * "
            + "FROM apply_record "
            + "<where> "
            + "<if test='applyRecord.applyUserId != null'>"
            + "applyUserId = #{applyRecord.applyUserId} "
            + "</if>"
            + "<if test='applyRecord.currentStatusRecordId != null'>"
            + "and currentStatusRecordId = #{applyRecord.currentStatusRecordId} "
            + "</if>"
            + "</where>"
            + "ORDER BY id "
            + "</script>")
    List<ApplyRecord> getApplyRecords(@Param("applyRecord") ApplyRecord applyRecord );
}