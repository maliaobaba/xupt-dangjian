package com.xupt.dangjian.mapper;

import com.xupt.dangjian.entity.Article;
import com.xupt.dangjian.entity.Proposal;
import com.xupt.dangjian.entity.ProposalExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

@Repository
public interface ProposalMapper {
    int countByExample(ProposalExample example);

    int deleteByExample(ProposalExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Proposal record);

    int insertSelective(Proposal record);

    List<Proposal> selectByExample(ProposalExample example);

    Proposal selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Proposal record, @Param("example") ProposalExample example);

    int updateByExample(@Param("record") Proposal record, @Param("example") ProposalExample example);

    int updateByPrimaryKeySelective(Proposal record);

    int updateByPrimaryKey(Proposal record);

    @Select("select * from proposal where type=#{type} order by id desc")
    List<Proposal> selectLatestProposalsByType(@Param("type")String param);
}