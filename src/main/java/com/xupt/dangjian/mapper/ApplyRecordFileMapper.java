package com.xupt.dangjian.mapper;

import com.xupt.dangjian.entity.ApplyRecordFile;
import com.xupt.dangjian.entity.ApplyRecordFileExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ApplyRecordFileMapper {
    int countByExample(ApplyRecordFileExample example);

    int deleteByExample(ApplyRecordFileExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(ApplyRecordFile record);

    int insertSelective(ApplyRecordFile record);

    List<ApplyRecordFile> selectByExample(ApplyRecordFileExample example);

    ApplyRecordFile selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") ApplyRecordFile record, @Param("example") ApplyRecordFileExample example);

    int updateByExample(@Param("record") ApplyRecordFile record, @Param("example") ApplyRecordFileExample example);

    int updateByPrimaryKeySelective(ApplyRecordFile record);

    int updateByPrimaryKey(ApplyRecordFile record);
}