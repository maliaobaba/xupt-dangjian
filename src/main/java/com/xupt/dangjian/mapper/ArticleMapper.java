package com.xupt.dangjian.mapper;

import com.xupt.dangjian.entity.Article;
import com.xupt.dangjian.entity.ArticleExample;
import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

@Repository
public interface ArticleMapper {
    int countByExample(ArticleExample example);

    int deleteByExample(ArticleExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Article record);

    int insertSelective(Article record);

    List<Article> selectByExample(ArticleExample example);

    Article selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Article record, @Param("example") ArticleExample example);

    int updateByExample(@Param("record") Article record, @Param("example") ArticleExample example);

    int updateByPrimaryKeySelective(Article record);

    int updateByPrimaryKey(Article record);

    @Select(
            {
                    "<script>",
                    "select * from article where id in",
                    "<foreach item='item' collection='articleIdList' open='(' separator=',' close=')'>",
                    "#{item}",
                    "</foreach>",
                    "</script>"
            }
    )
    List<Article> getArticleList(@Param("articleIdList") List<Integer> articleIdList) throws Exception;

    @Delete(
            {
                    "<script>",
                    "DELETE FROM article where article.id in",
                    "<foreach item='item' collection='articleIdList' open='(' separator=',' close=')'>",
                    "#{item}",
                    "</foreach>",
                    "</script>"
            }
    )
    Integer deleteArticles(@Param("articleIdList") List<Integer> articleIdList) throws Exception;

    @Select("select * from article where articleClass=#{articleClass} order by id desc")
    List<Article> selectByClass(@Param("articleClass") String articleClass) throws Exception;

    @Select("select * from article order by id desc")
    List<Article> selectLatestArticles() throws Exception;

    @Select("select * from article where title like '%${key}%'")
    List<Article> fuzzySearchByTitle(@Param("key") String key) throws Exception;
}