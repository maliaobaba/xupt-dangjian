package com.xupt.dangjian.mapper;

import com.xupt.dangjian.entity.User;
import com.xupt.dangjian.entity.UserExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

@Repository
public interface UserMapper {
    int countByExample(UserExample example);

    int deleteByExample(UserExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(User record);

    int insertSelective(User record);

    List<User> selectByExample(UserExample example);

    User selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") User record, @Param("example") UserExample example);

    int updateByExample(@Param("record") User record, @Param("example") UserExample example);

    int updateByPrimaryKeySelective(User record);

    int updateByPrimaryKey(User record);

    @Select("select * from user where id=#{id}")
    User getUserById(@Param("id") Integer id) throws Exception;

    @Select("select * from user where sid=#{sid}")
    User getUserBySid(@Param("sid") String sid) throws Exception;

    @Select( "select r.name from user_role as ur,role as r where ur.userId=#{userId} and r.id=ur.roleId")
    List<String> getRolesById(@Param( "userId" ) Integer userId);
}