package com.xupt.dangjian.mapper;

import com.xupt.dangjian.entity.Quotation;
import com.xupt.dangjian.entity.QuotationExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

@Repository
public interface QuotationMapper {
    int countByExample(QuotationExample example);

    int deleteByExample(QuotationExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Quotation record);

    int insertSelective(Quotation record);

    List<Quotation> selectByExample(QuotationExample example);

    Quotation selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Quotation record, @Param("example") QuotationExample example);

    int updateByExample(@Param("record") Quotation record, @Param("example") QuotationExample example);

    int updateByPrimaryKeySelective(Quotation record);

    int updateByPrimaryKey(Quotation record);

    @Select("select * from quotation order by id desc")
    List<Quotation> getQuotations();

    @Select("select top 1 * from quotation order by id desc")
    Quotation getLatest();
}