package com.xupt.dangjian.mapper;

import com.xupt.dangjian.entity.Dangzhan;
import com.xupt.dangjian.entity.DangzhanExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface DangzhanMapper {
    int countByExample(DangzhanExample example);

    int deleteByExample(DangzhanExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Dangzhan record);

    int insertSelective(Dangzhan record);

    List<Dangzhan> selectByExample(DangzhanExample example);

    Dangzhan selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Dangzhan record, @Param("example") DangzhanExample example);

    int updateByExample(@Param("record") Dangzhan record, @Param("example") DangzhanExample example);

    int updateByPrimaryKeySelective(Dangzhan record);

    int updateByPrimaryKey(Dangzhan record);
}