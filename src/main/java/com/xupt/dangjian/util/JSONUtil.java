package com.xupt.dangjian.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.Map;

public class JSONUtil {
    public static ObjectMapper objectMapper=new ObjectMapper();
    public static <T> T parseMap(Object object, Class<T> tClass) throws Exception {
        String json=objectMapper.writeValueAsString(object);
        T t=objectMapper.readValue(json,tClass);
        return t;
    }

}
