package com.xupt.dangjian.util;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringUtil {
    public static List<String> matchAll(String regex, String str) {

        if(str==null)
            return null;
        List<String> list = new ArrayList<>();
        //将规则封装成对象。
        Pattern p = Pattern.compile( regex );

        //让正则对象和要作用的字符串相关联。获取匹配器对象。
        Matcher m = p.matcher( str );

        while (m.find()) {
            list.add( m.group().substring( 7 ) );
            // start()  字符的开始下标（包含）
            //end()  字符的结束下标（不包含）

        }

        return list;
    }
}
