package com.xupt.dangjian;

import com.github.tobato.fastdfs.FdfsClientConfig;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.EnableMBeanExport;
import org.springframework.context.annotation.Import;
import org.springframework.jmx.support.RegistrationPolicy;

@Import(FdfsClientConfig.class)
@SpringBootApplication
@MapperScan("com.xupt.dangjian.mapper")
@EnableMBeanExport(registration = RegistrationPolicy.IGNORE_EXISTING)

public class DangjianApplication {
	public static void main(String[] args) {
		SpringApplication.run(DangjianApplication.class, args);
	}
}
