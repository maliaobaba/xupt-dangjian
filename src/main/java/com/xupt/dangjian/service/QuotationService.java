package com.xupt.dangjian.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.xupt.dangjian.entity.Quotation;

public interface QuotationService {

	public abstract void addQuotation(Quotation quotation);

	public abstract JsonNode getQuotations(JsonNode pageModel);

	public abstract Quotation getLatestQuotation();

	public abstract void deleteQuotation(Integer id);
}
