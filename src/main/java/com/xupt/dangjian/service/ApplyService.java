package com.xupt.dangjian.service;

import com.sun.prism.impl.Disposer;
import com.xupt.dangjian.entity.ApplyRecord;

import java.util.HashMap;
import java.util.List;

public interface ApplyService {

	public abstract void addApplyRecord(ApplyRecord applyRecord);
	public abstract void deleteApplyRecord(Integer id);
	public abstract void updateApplyRecord(ApplyRecord applyRecord);
	public abstract List<ApplyRecord> getApplyRecords(ApplyRecord applyRecord);
}
