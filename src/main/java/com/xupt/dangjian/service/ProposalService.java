package com.xupt.dangjian.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.xupt.dangjian.entity.Proposal;
import com.xupt.dangjian.model.PageModel;
import com.xupt.dangjian.service.impl.ProposalServiceImpl;
import org.springframework.stereotype.Service;

@Service
public interface ProposalService {

	public Proposal getProposalById(Integer id);

	public void submitProposal(Proposal proposal);

	public JsonNode getLatestProposals(JsonNode pageModel);
}
