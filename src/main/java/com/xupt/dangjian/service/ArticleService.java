package com.xupt.dangjian.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.xupt.dangjian.entity.Article;
import com.xupt.dangjian.entity.File;
import com.xupt.dangjian.model.PageModel;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface  ArticleService {
  public void addArticle(Article article) throws Exception;

  public List<File> addFiles(MultipartFile[] multipartFiles) throws Exception;

  public JsonNode getArticles(JsonNode pageModel) throws Exception;

  public Article getArticle(Integer articleId) throws  Exception;

  public void deleteArticles(List<Integer> articleIdList) throws Exception;

  public void updateArticle(Article article) throws Exception;

  public JsonNode getLatestArticles(JsonNode pageModel) throws Exception;

  public JsonNode fuzzySearch(JsonNode pageModel) throws Exception;

  public File getFileById(Integer id) throws Exception;
}
