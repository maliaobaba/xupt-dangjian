package com.xupt.dangjian.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.xupt.dangjian.entity.Link;
import com.xupt.dangjian.model.PageModel;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public interface EducationService {
    public void releaseLink(Link link) throws Exception;

    public void deleteLink(List<Link> links) throws Exception;

    public JsonNode getLatestLinks(JsonNode pageModel) throws Exception;

    public JsonNode getLinksByClass(JsonNode pageModel);

}
