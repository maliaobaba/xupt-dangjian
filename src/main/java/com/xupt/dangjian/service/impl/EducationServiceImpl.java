package com.xupt.dangjian.service.impl;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.xupt.dangjian.entity.Link;
import com.xupt.dangjian.mapper.LinkMapper;
import com.xupt.dangjian.model.PageModel;
import com.xupt.dangjian.service.EducationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class EducationServiceImpl implements EducationService{

    @Autowired
    LinkMapper linkMapper;
    @Override
    public void releaseLink(Link link) throws Exception {
        linkMapper.insert( link );
    }

    @Override
    public void deleteLink(List<Link> links) throws Exception {
        for(Link link:links)
        {
            linkMapper.deleteByPrimaryKey( link.getId() );
        }
    }

    @Override
    public JsonNode getLatestLinks(JsonNode pageModel) throws Exception {
        PageHelper.startPage( pageModel.path("pageNum").asInt(),pageModel.path("pageSize").asInt() );
        List linkList=linkMapper.selectAllDesc();
        PageInfo pageInfo = new PageInfo(linkList);
        ((ObjectNode)pageModel).putPOJO("linkList",linkList);
        ((ObjectNode)pageModel).putPOJO("total",pageInfo.getTotal());
        return pageModel;
    }

    @Override
    public JsonNode getLinksByClass(JsonNode pageModel) {
        PageHelper.startPage(pageModel.path("pageNum").asInt(),pageModel.path("pageSize").asInt());
        List linkList=linkMapper.selectByClass(pageModel.path("linkClass").asText());
        PageInfo pageInfo = new PageInfo(linkList);
        ((ObjectNode)pageModel).putPOJO("linkList",linkList);
        ((ObjectNode)pageModel).putPOJO("total",pageInfo.getTotal());
        return pageModel;

    }
}
