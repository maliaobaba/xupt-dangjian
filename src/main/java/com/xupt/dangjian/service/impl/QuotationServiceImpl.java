package com.xupt.dangjian.service.impl;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.xupt.dangjian.entity.Quotation;
import com.xupt.dangjian.mapper.QuotationMapper;
import com.xupt.dangjian.service.QuotationService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class QuotationServiceImpl implements QuotationService {

	@Autowired
	QuotationMapper quotationMapper;

	@Override
	public void addQuotation(Quotation quotation) {

		quotationMapper.insert(quotation);
	}

	@Override
	public JsonNode getQuotations(JsonNode pageModel) {

		PageHelper.startPage(pageModel.path("pageNum").asInt(), pageModel.path("pageSize").asInt());
		List<Quotation> quotationList=quotationMapper.getQuotations();
		PageInfo pageInfo = new PageInfo(quotationList);
		((ObjectNode)pageModel).putPOJO("proposalList",quotationList);
		((ObjectNode)pageModel).putPOJO("total",pageInfo.getTotal());
		return pageModel;
	}

	@Override
	public Quotation getLatestQuotation() {
		return quotationMapper.getLatest();
	}

	@Override
	public void deleteQuotation(Integer id) {
		quotationMapper.deleteByPrimaryKey(id);
	}
}
