package com.xupt.dangjian.service.impl;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.xupt.dangjian.entity.Article;
import com.xupt.dangjian.entity.File;
import com.xupt.dangjian.exception.MyException;
import com.xupt.dangjian.mapper.ArticleMapper;
import com.xupt.dangjian.mapper.FileMapper;
import com.xupt.dangjian.model.PageModel;
import com.xupt.dangjian.service.ArticleService;
import com.xupt.dangjian.util.FileUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.util.Date;
import java.util.List;


@Service
public class ArticleServiceImpl implements ArticleService {

    @Autowired
    ArticleMapper articleMapper;
    @Autowired
    FileUtil fileUtil;
    @Autowired
    FileMapper fileMapper;

    @Transactional
    public void addArticle(Article article) throws Exception {
        article.setCreatedTime(new Date());
        article.setViewTimes(0);
        articleMapper.insert(article);
    }

    @Override
    public List<File> addFiles(MultipartFile[] multipartFiles) throws Exception {
        List<File> fileList = fileUtil.filesUpLoad(multipartFiles);
        fileMapper.addFiles(fileList);
        return fileList;
    }

    @Override
    public JsonNode getArticles(JsonNode pageModel) throws Exception {
        PageHelper.startPage(pageModel.path("pageNum").asInt(), pageModel.path("pageSize").asInt());
        String articleClass=pageModel.path("articleClass").asText();
        List articleList = articleMapper.selectByClass(articleClass);
        PageInfo pageInfo = new PageInfo(articleList);
        ((ObjectNode)pageModel).putPOJO("articleList",articleList);
        ((ObjectNode)pageModel).putPOJO("total",pageInfo.getTotal());
        return pageModel;
    }

    @Override
    public Article getArticle(Integer articleId) throws Exception {
        Article article = articleMapper.selectByPrimaryKey(articleId);
        if (article == null)
            throw new MyException("文章不存在");
        article.setViewTimes(article.getViewTimes() + 1);
        articleMapper.updateByPrimaryKey(article);
        return article;
    }

    @Transactional
    @Override
    public void deleteArticles(List<Integer> articleIdList) throws Exception {
        articleMapper.deleteArticles(articleIdList);
    }

    @Transactional
    @Override
    public void updateArticle(Article article) throws Exception {
        articleMapper.updateByPrimaryKeySelective(article);
    }

    @Override
    public JsonNode getLatestArticles(JsonNode pageModel) throws Exception {
	    PageHelper.startPage(pageModel.path("pageNum").asInt(), pageModel.path("pageSize").asInt());
        List articleList = articleMapper.selectLatestArticles();
        PageInfo pageInfo = new PageInfo(articleList);
	    ((ObjectNode)pageModel).putPOJO("articleList",articleList);
	    ((ObjectNode)pageModel).putPOJO("total",pageInfo.getTotal());
        return pageModel;
    }

    @Override
    public JsonNode fuzzySearch(JsonNode pageModel) throws Exception {
	    PageHelper.startPage(pageModel.path("pageNum").asInt(), pageModel.path("pageSize").asInt());
        List articleList = articleMapper.fuzzySearchByTitle(pageModel.path("key").asText());
        PageInfo pageInfo = new PageInfo(articleList);
	    ((ObjectNode)pageModel).putPOJO("articleList",articleList);
	    ((ObjectNode)pageModel).putPOJO("total",pageInfo.getTotal());
        return pageModel;
    }

    @Override
    public File getFileById(Integer id) throws Exception {
        File file=fileMapper.selectByPrimaryKey(id);
        return file;
    }
}
