package com.xupt.dangjian.service.impl;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.xupt.dangjian.entity.Proposal;
import com.xupt.dangjian.mapper.ProposalMapper;
import com.xupt.dangjian.model.PageModel;
import com.xupt.dangjian.service.ProposalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProposalServiceImpl implements ProposalService {

	@Autowired
	ProposalMapper proposalMapper;

	public Proposal getProposalById(Integer id){

		return proposalMapper.selectByPrimaryKey(id);

	}

	@Override
	public void submitProposal(Proposal proposal) {
      proposalMapper.insert(proposal);
	}

	@Override
	public JsonNode getLatestProposals(JsonNode pageModel) {
		PageHelper.startPage(pageModel.path("pageNum").asInt(), pageModel.path("pageSize").asInt());
		List proposalList = proposalMapper.selectLatestProposalsByType(pageModel.path("type").asText());
		PageInfo pageInfo = new PageInfo(proposalList);
		((ObjectNode)pageModel).putPOJO("proposalList",proposalList);
		((ObjectNode)pageModel).putPOJO("total",pageInfo.getTotal());
		return pageModel;
	}
}
