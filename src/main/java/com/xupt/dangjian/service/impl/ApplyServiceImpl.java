package com.xupt.dangjian.service.impl;

import com.xupt.dangjian.entity.ApplyRecord;
import com.xupt.dangjian.mapper.ApplyRecordMapper;
import com.xupt.dangjian.service.ApplyService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashMap;
import java.util.List;

public class ApplyServiceImpl implements ApplyService {

	@Autowired
	ApplyRecordMapper applyRecordMapper;


	@Override
	public void addApplyRecord(ApplyRecord applyRecord) {
      applyRecordMapper.insert(applyRecord) ;
	}

	@Override
	public void deleteApplyRecord(Integer id) {
     applyRecordMapper.deleteByPrimaryKey(id);
	}

	@Override
	public void updateApplyRecord(ApplyRecord applyRecord) {
     applyRecordMapper.updateByPrimaryKeySelective(applyRecord);
	}

	@Override
	public List<ApplyRecord> getApplyRecords(ApplyRecord applyRecord) {
		return applyRecordMapper.getApplyRecords(applyRecord);
	}
}
