package com.xupt.dangjian.service.impl;

import com.xupt.dangjian.entity.User;
import com.xupt.dangjian.exception.MyException;
import com.xupt.dangjian.mapper.UserMapper;
import com.xupt.dangjian.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserMapper userMapper;

    @Override
    public User login(String sid, String password) throws Exception {
        User user = userMapper.getUserBySid( sid );
        /*if(user==null)
            throw new MyException( "系统中没有该用户" );
        if(user.getPassword().equals( password ))
            return user;
        else
            throw new MyException("用户密码错误");*/

        if (StringUtils.isEmpty( user ) || !(user.getPassword().equals( password ))) {
            throw new MyException( "用户或密码错误，请重试" );
        } else {
            return user;
        }

    }

    @Override
    public User getUserById(Integer userId) throws Exception {
        return userMapper.getUserById( userId );
    }

    @Override
    public List<String> getRolesById(Integer userId) throws Exception {
        return userMapper.getRolesById( userId );
    }

    @Override
    public void register(User user) throws Exception {
        User userIn = userMapper.getUserBySid( user.getSid() );
        if (userIn == null)
            userMapper.insert( user );
        else
            throw new MyException( "学号已经被注册" );
    }

    @Override
    public void updateUser(User user) throws Exception {
        userMapper.updateByPrimaryKeySelective(user);
    }


}
