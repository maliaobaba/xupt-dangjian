package com.xupt.dangjian.service;

import com.xupt.dangjian.entity.User;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface UserService {

    public User login(String sid, String password) throws Exception;
    public User getUserById(Integer userId) throws Exception;
    public List<String> getRolesById(Integer userId) throws Exception;
    public void register(User user) throws  Exception;
    public void updateUser(User user) throws Exception;
}
