package com.xupt.dangjian.controller;

import com.fasterxml.jackson.databind.JsonNode;
import com.xupt.dangjian.entity.Link;
import com.xupt.dangjian.exception.MyException;
import com.xupt.dangjian.model.PageModel;
import com.xupt.dangjian.model.ResponseModel;
import com.xupt.dangjian.service.EducationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@Controller
@RequestMapping("/education")
public class EducationController {

    @Autowired
    EducationService educationService;

    @RequestMapping(value = "/releaseLink",method = RequestMethod.POST)
    @ResponseBody
    public ResponseModel release(@RequestBody Link link) throws Exception {

        ResponseModel responseModel = new ResponseModel();
        responseModel.setStatus( 1 );
        try {
            educationService.releaseLink( link );
        } catch (Exception e) {
            responseModel.setStatus( 0 );
            if (e instanceof MyException)
                responseModel.setMessage( e.getMessage() );
            else
                responseModel.setMessage( "后台未知异常" );
            e.printStackTrace();

        }
        return responseModel;
    }

    @RequestMapping(value = "/deleteLink",method = RequestMethod.POST)
    @ResponseBody
    public ResponseModel delete(@RequestBody List<Link> links) throws Exception {

        ResponseModel responseModel = new ResponseModel();
        responseModel.setStatus( 1 );
        try {
            educationService.deleteLink( links );
        } catch (Exception e) {
            responseModel.setStatus( 0 );
            if (e instanceof MyException)
                responseModel.setMessage( e.getMessage() );
            else
                responseModel.setMessage( "后台未知异常" );
            e.printStackTrace();

        }
        return responseModel;
    }

    @RequestMapping(value = "/getLatestLinks",method = RequestMethod.POST)
    @ResponseBody
    public ResponseModel getLatestLinks(@RequestBody PageModel pageModel) throws Exception {

        ResponseModel responseModel = new ResponseModel();
        responseModel.setStatus( 1 );
        try {
            pageModel=educationService.getLatestLinks( pageModel );
            responseModel.setData( pageModel );
        } catch (Exception e) {
            responseModel.setStatus( 0 );
            if (e instanceof MyException)
                responseModel.setMessage( e.getMessage() );
            else
                responseModel.setMessage( "后台未知异常" );
            e.printStackTrace();

        }
        return responseModel;
    }


    @RequestMapping(value = "/getLinks",method = RequestMethod.POST)
    @ResponseBody
    public ResponseModel updateArticle(@RequestBody JsonNode pageModelAndclass) throws Exception {

        ResponseModel responseModel = new ResponseModel();
        responseModel.setStatus( 1 );
        try {
            JsonNode pageModelNode= pageModelAndclass.path("pageModel");
            PageModel pageModel=new PageModel();
            pageModel.setPageNum(pageModelNode.path("pageNum").asInt());
            pageModel.setPageSize(pageModelNode.path("pageSize").asInt());
            String linkClass= pageModelAndclass.path("linkClass").asText();
            pageModel=educationService.getLinksByClass( pageModel,linkClass );
            responseModel.setData( pageModel );
        } catch (Exception e) {
            responseModel.setStatus( 0 );
            if (e instanceof MyException)
                responseModel.setMessage( e.getMessage() );
            else
                responseModel.setMessage( "后台未知异常" );
            e.printStackTrace();

        }
        return responseModel;
    }
}
