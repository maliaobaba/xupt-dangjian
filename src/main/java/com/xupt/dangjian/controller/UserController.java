package com.xupt.dangjian.controller;

import com.xupt.dangjian.entity.User;
import com.xupt.dangjian.exception.MyException;
import com.xupt.dangjian.model.ResponseModel;
import com.xupt.dangjian.service.UserService;
import com.xupt.dangjian.util.JSRUtil;
import com.xupt.dangjian.util.TokenUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

@CrossOrigin
@Controller
@RequestMapping("/user")
public class UserController {
    @Autowired
    UserService userService;

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    @ResponseBody
    public ResponseModel login(@RequestBody User user, HttpServletResponse response) throws Exception {

        ResponseModel responseModel = new ResponseModel();
        responseModel.setStatus(1);
        try {
            user = userService.login(user.getSid(), user.getPassword());
            String token = TokenUtil.getToken(user);

            @Getter
            @Setter
            class TokenAndUser {
                User user;
                String token;

                public TokenAndUser(User user, String token) {
                    this.token = token;
                    this.user = user;
                }

                ;
            }
            responseModel.setData(new TokenAndUser(user, token));
        } catch (Exception e) {
            responseModel.setStatus(0);
            if (e instanceof MyException)
                responseModel.setMessage(e.getMessage());
            else
                responseModel.setMessage("后台未知异常");
        }
        return responseModel;


        //定义方法内部类组合个人信息与token

    }


    @RequestMapping(value = "/register", method = RequestMethod.POST)
    @ResponseBody
    public ResponseModel register(@Valid @RequestBody User user, BindingResult bindingResult) {
        ResponseModel responseModel = new ResponseModel();
        responseModel.setStatus(1);
        if (bindingResult.hasErrors()) {
            responseModel.setParameterError(JSRUtil.getAllMessage(bindingResult));
            responseModel.setStatus(0);
            return responseModel;
        }

        try {
            userService.register(user);
            String token = TokenUtil.getToken(user);
            @Getter
            @Setter
            class TokenAndUser {
                User user;
                String token;

                public TokenAndUser(User user, String token) {
                    this.token = token;
                    this.user = user;
                }
            }
            responseModel.setData(new TokenAndUser(user, token));
        } catch (Exception e) {
            responseModel.setStatus(0);
            if (e instanceof MyException)
                responseModel.setMessage(e.getMessage());
            else
                responseModel.setMessage("后台未知异常");

        }
        return responseModel;
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @ResponseBody
    public ResponseModel update(@RequestBody User user) throws Exception {

        ResponseModel responseModel = new ResponseModel();
        responseModel.setStatus(1);
        try {
           userService.updateUser(user);
        } catch (Exception e) {
            responseModel.setStatus(0);
            if (e instanceof MyException)
                responseModel.setMessage(e.getMessage());
            else
                responseModel.setMessage("后台未知异常");
        }
        return responseModel;
    }
}
