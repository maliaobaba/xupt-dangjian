package com.xupt.dangjian.controller;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.xupt.dangjian.entity.Article;
import com.xupt.dangjian.entity.File;
import com.xupt.dangjian.exception.MyException;
import com.xupt.dangjian.model.PageModel;
import com.xupt.dangjian.model.ResponseModel;
import com.xupt.dangjian.service.ArticleService;
import com.xupt.dangjian.util.JSONUtil;
import com.xupt.dangjian.util.JSRUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@CrossOrigin
@Controller
@RequestMapping("/article")
public class ArticleController {

    private static final ObjectMapper mapper = new ObjectMapper().setSerializationInclusion(JsonInclude.Include.NON_NULL);

    @Autowired
    ArticleService articleService;

    @RequestMapping(value = "/release", method = RequestMethod.POST)
    @ResponseBody
    public ResponseModel release(@Valid @RequestBody Article article, BindingResult bindingResult) {
        ResponseModel responseModel = new ResponseModel();
        responseModel.setStatus(1);
        if (bindingResult.hasErrors()) {
            responseModel.setParameterError(JSRUtil.getAllMessage(bindingResult));
            responseModel.setStatus(0);
            return responseModel;
        }
        try {
            articleService.addArticle(article);
        } catch (Exception e) {
            responseModel.setStatus(0);
            if (e instanceof MyException)
                responseModel.setMessage(e.getMessage());
            else
                responseModel.setMessage("后台未知异常");
            e.printStackTrace();
        }
        return responseModel;
    }

    @RequestMapping(value = "/pictureUpload", method = RequestMethod.POST)
    @ResponseBody
    public ObjectNode pictureUpload(@RequestParam("files") MultipartFile[] multipartFiles) throws Exception {
        ObjectNode responseNode=mapper.createObjectNode();
        try {
            List<File> fileList = articleService.addFiles(multipartFiles);
            List<String> urlList = new ArrayList<String>();
            for (File file : fileList) {
                urlList.add("http://" + file.getUrl());
            }
            responseNode.put("errno",0);
            responseNode.putPOJO("data",urlList);
        } catch (Exception e) {
            responseNode.put("errno",1);
        }
        return responseNode;
    }


    @RequestMapping(value = "/fileUpload", method = RequestMethod.POST)
    @ResponseBody
    public ResponseModel fileUpload(@RequestParam("files") MultipartFile[] multipartFiles) throws Exception {

        ResponseModel responseModel = new ResponseModel();
        responseModel.setStatus(1);
        try {
            List<File> fileList = articleService.addFiles(multipartFiles);
            responseModel.setData(fileList);
        } catch (Exception e) {
            responseModel.setStatus(0);
            if (e instanceof MyException)
                responseModel.setMessage(e.getMessage());
            else
                responseModel.setMessage("后台未知异常");
            e.printStackTrace();
        }
        return responseModel;
    }

    @RequestMapping(value = "/getNews", method = RequestMethod.POST)
    @ResponseBody
    public ResponseModel getNews(@Valid @RequestBody Map<String, Object> map, BindingResult bindingResult) {
        ResponseModel responseModel = new ResponseModel();
        responseModel.setStatus(1);
        if (bindingResult.hasErrors()) {
            responseModel.setParameterError(JSRUtil.getAllMessage(bindingResult));
            responseModel.setStatus(0);
            return responseModel;
        }
        try {
            PageModel pageModel = JSONUtil.parseMap(map.get("pageModel"), PageModel.class);
            String articleClass = JSONUtil.parseMap(map.get("articleClass"), String.class);
            pageModel = articleService.getArticles(pageModel, articleClass);
            responseModel.setData(pageModel);
        } catch (Exception e) {
            responseModel.setStatus(0);
            if (e instanceof MyException)
                responseModel.setMessage(e.getMessage());
            else
                responseModel.setMessage("后台未知异常");
            e.printStackTrace();
        }
        return responseModel;
    }

    @RequestMapping(value = "/getOneNew", method = RequestMethod.POST)
    @ResponseBody
    public ResponseModel getOneNew(@RequestBody Article article) throws Exception {

        ResponseModel responseModel = new ResponseModel();
        responseModel.setStatus(1);
        try {
            article = articleService.getArticle(article.getId());
            responseModel.setData(article);
        } catch (Exception e) {
            responseModel.setStatus(0);
            if (e instanceof MyException)
                responseModel.setMessage(e.getMessage());
            else
                responseModel.setMessage("后台未知异常");
            e.printStackTrace();

        }
        return responseModel;
    }


    @RequestMapping(value = "/deleteArticle", method = RequestMethod.POST)
    @ResponseBody
    public ResponseModel deleteArticle(@RequestBody List<Integer> articleIdList) throws Exception {

        ResponseModel responseModel = new ResponseModel();
        responseModel.setStatus(1);
        try {
            articleService.deleteArticles(articleIdList);
        } catch (Exception e) {
            responseModel.setStatus(0);
            if (e instanceof MyException)
                responseModel.setMessage(e.getMessage());
            else
                responseModel.setMessage("后台未知异常");
            e.printStackTrace();
        }
        return responseModel;
    }


    @RequestMapping(value = "/updateArticle", method = RequestMethod.POST)
    @ResponseBody
    public ResponseModel updateArticle(@RequestBody Article article) throws Exception {

        ResponseModel responseModel = new ResponseModel();
        responseModel.setStatus(1);
        try {
            articleService.updateArticle(article);
        } catch (Exception e) {
            responseModel.setStatus(0);
            if (e instanceof MyException)
                responseModel.setMessage(e.getMessage());
            else
                responseModel.setMessage("后台未知异常");
            e.printStackTrace();

        }
        return responseModel;
    }


    @RequestMapping(value = "/getLatestNews", method = RequestMethod.POST)
    @ResponseBody
    public ResponseModel getLatestNews(@RequestBody PageModel pageModel) throws Exception {

        ResponseModel responseModel = new ResponseModel();
        responseModel.setStatus(1);
        try {
            pageModel = articleService.getLatestArticles(pageModel);
            responseModel.setData(pageModel);
        } catch (Exception e) {
            responseModel.setStatus(0);
            if (e instanceof MyException)
                responseModel.setMessage(e.getMessage());
            else
                responseModel.setMessage("后台未知异常");
            e.printStackTrace();

        }
        return responseModel;
    }

    @RequestMapping(value = "/homeSearch", method = RequestMethod.POST)
    @ResponseBody
    public ResponseModel homeSearch(@RequestBody Map<String, Object> map) throws Exception {

        PageModel pageModel = JSONUtil.parseMap(map.get("pageModel"), PageModel.class);
        String key = JSONUtil.parseMap(map.get("key"), String.class);
        ResponseModel responseModel = new ResponseModel();
        responseModel.setStatus(1);
        try {
            pageModel = articleService.fuzzySearch(pageModel,key);
            responseModel.setData(pageModel);
        } catch (Exception e) {
            responseModel.setStatus(0);
            if (e instanceof MyException)
                responseModel.setMessage(e.getMessage());
            else
                responseModel.setMessage("后台未知异常");
            e.printStackTrace();

        }
        return responseModel;
    }

    @RequestMapping(value = "/getFileById", method = RequestMethod.POST)
    @ResponseBody
    public ResponseModel getFileById(@RequestBody JsonNode node) throws Exception {

        Integer id=node.path("id").asInt();
        ResponseModel responseModel = new ResponseModel();
        responseModel.setStatus(1);
        try {
            File file = articleService.getFileById(id);
            responseModel.setData(file);
        } catch (Exception e) {
            responseModel.setStatus(0);
            if (e instanceof MyException)
                responseModel.setMessage(e.getMessage());
            else
                responseModel.setMessage("后台未知异常");
            e.printStackTrace();

        }
        return responseModel;
    }
}


