package com.xupt.dangjian.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class StatusRecordExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public StatusRecordExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andStatusIdIsNull() {
            addCriterion("statusId is null");
            return (Criteria) this;
        }

        public Criteria andStatusIdIsNotNull() {
            addCriterion("statusId is not null");
            return (Criteria) this;
        }

        public Criteria andStatusIdEqualTo(Integer value) {
            addCriterion("statusId =", value, "statusId");
            return (Criteria) this;
        }

        public Criteria andStatusIdNotEqualTo(Integer value) {
            addCriterion("statusId <>", value, "statusId");
            return (Criteria) this;
        }

        public Criteria andStatusIdGreaterThan(Integer value) {
            addCriterion("statusId >", value, "statusId");
            return (Criteria) this;
        }

        public Criteria andStatusIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("statusId >=", value, "statusId");
            return (Criteria) this;
        }

        public Criteria andStatusIdLessThan(Integer value) {
            addCriterion("statusId <", value, "statusId");
            return (Criteria) this;
        }

        public Criteria andStatusIdLessThanOrEqualTo(Integer value) {
            addCriterion("statusId <=", value, "statusId");
            return (Criteria) this;
        }

        public Criteria andStatusIdIn(List<Integer> values) {
            addCriterion("statusId in", values, "statusId");
            return (Criteria) this;
        }

        public Criteria andStatusIdNotIn(List<Integer> values) {
            addCriterion("statusId not in", values, "statusId");
            return (Criteria) this;
        }

        public Criteria andStatusIdBetween(Integer value1, Integer value2) {
            addCriterion("statusId between", value1, value2, "statusId");
            return (Criteria) this;
        }

        public Criteria andStatusIdNotBetween(Integer value1, Integer value2) {
            addCriterion("statusId not between", value1, value2, "statusId");
            return (Criteria) this;
        }

        public Criteria andPreRecordIdIsNull() {
            addCriterion("preRecordId is null");
            return (Criteria) this;
        }

        public Criteria andPreRecordIdIsNotNull() {
            addCriterion("preRecordId is not null");
            return (Criteria) this;
        }

        public Criteria andPreRecordIdEqualTo(Integer value) {
            addCriterion("preRecordId =", value, "preRecordId");
            return (Criteria) this;
        }

        public Criteria andPreRecordIdNotEqualTo(Integer value) {
            addCriterion("preRecordId <>", value, "preRecordId");
            return (Criteria) this;
        }

        public Criteria andPreRecordIdGreaterThan(Integer value) {
            addCriterion("preRecordId >", value, "preRecordId");
            return (Criteria) this;
        }

        public Criteria andPreRecordIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("preRecordId >=", value, "preRecordId");
            return (Criteria) this;
        }

        public Criteria andPreRecordIdLessThan(Integer value) {
            addCriterion("preRecordId <", value, "preRecordId");
            return (Criteria) this;
        }

        public Criteria andPreRecordIdLessThanOrEqualTo(Integer value) {
            addCriterion("preRecordId <=", value, "preRecordId");
            return (Criteria) this;
        }

        public Criteria andPreRecordIdIn(List<Integer> values) {
            addCriterion("preRecordId in", values, "preRecordId");
            return (Criteria) this;
        }

        public Criteria andPreRecordIdNotIn(List<Integer> values) {
            addCriterion("preRecordId not in", values, "preRecordId");
            return (Criteria) this;
        }

        public Criteria andPreRecordIdBetween(Integer value1, Integer value2) {
            addCriterion("preRecordId between", value1, value2, "preRecordId");
            return (Criteria) this;
        }

        public Criteria andPreRecordIdNotBetween(Integer value1, Integer value2) {
            addCriterion("preRecordId not between", value1, value2, "preRecordId");
            return (Criteria) this;
        }

        public Criteria andNextRecordIdIsNull() {
            addCriterion("nextRecordId is null");
            return (Criteria) this;
        }

        public Criteria andNextRecordIdIsNotNull() {
            addCriterion("nextRecordId is not null");
            return (Criteria) this;
        }

        public Criteria andNextRecordIdEqualTo(Integer value) {
            addCriterion("nextRecordId =", value, "nextRecordId");
            return (Criteria) this;
        }

        public Criteria andNextRecordIdNotEqualTo(Integer value) {
            addCriterion("nextRecordId <>", value, "nextRecordId");
            return (Criteria) this;
        }

        public Criteria andNextRecordIdGreaterThan(Integer value) {
            addCriterion("nextRecordId >", value, "nextRecordId");
            return (Criteria) this;
        }

        public Criteria andNextRecordIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("nextRecordId >=", value, "nextRecordId");
            return (Criteria) this;
        }

        public Criteria andNextRecordIdLessThan(Integer value) {
            addCriterion("nextRecordId <", value, "nextRecordId");
            return (Criteria) this;
        }

        public Criteria andNextRecordIdLessThanOrEqualTo(Integer value) {
            addCriterion("nextRecordId <=", value, "nextRecordId");
            return (Criteria) this;
        }

        public Criteria andNextRecordIdIn(List<Integer> values) {
            addCriterion("nextRecordId in", values, "nextRecordId");
            return (Criteria) this;
        }

        public Criteria andNextRecordIdNotIn(List<Integer> values) {
            addCriterion("nextRecordId not in", values, "nextRecordId");
            return (Criteria) this;
        }

        public Criteria andNextRecordIdBetween(Integer value1, Integer value2) {
            addCriterion("nextRecordId between", value1, value2, "nextRecordId");
            return (Criteria) this;
        }

        public Criteria andNextRecordIdNotBetween(Integer value1, Integer value2) {
            addCriterion("nextRecordId not between", value1, value2, "nextRecordId");
            return (Criteria) this;
        }

        public Criteria andApplyRecordIdIsNull() {
            addCriterion("applyRecordId is null");
            return (Criteria) this;
        }

        public Criteria andApplyRecordIdIsNotNull() {
            addCriterion("applyRecordId is not null");
            return (Criteria) this;
        }

        public Criteria andApplyRecordIdEqualTo(Integer value) {
            addCriterion("applyRecordId =", value, "applyRecordId");
            return (Criteria) this;
        }

        public Criteria andApplyRecordIdNotEqualTo(Integer value) {
            addCriterion("applyRecordId <>", value, "applyRecordId");
            return (Criteria) this;
        }

        public Criteria andApplyRecordIdGreaterThan(Integer value) {
            addCriterion("applyRecordId >", value, "applyRecordId");
            return (Criteria) this;
        }

        public Criteria andApplyRecordIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("applyRecordId >=", value, "applyRecordId");
            return (Criteria) this;
        }

        public Criteria andApplyRecordIdLessThan(Integer value) {
            addCriterion("applyRecordId <", value, "applyRecordId");
            return (Criteria) this;
        }

        public Criteria andApplyRecordIdLessThanOrEqualTo(Integer value) {
            addCriterion("applyRecordId <=", value, "applyRecordId");
            return (Criteria) this;
        }

        public Criteria andApplyRecordIdIn(List<Integer> values) {
            addCriterion("applyRecordId in", values, "applyRecordId");
            return (Criteria) this;
        }

        public Criteria andApplyRecordIdNotIn(List<Integer> values) {
            addCriterion("applyRecordId not in", values, "applyRecordId");
            return (Criteria) this;
        }

        public Criteria andApplyRecordIdBetween(Integer value1, Integer value2) {
            addCriterion("applyRecordId between", value1, value2, "applyRecordId");
            return (Criteria) this;
        }

        public Criteria andApplyRecordIdNotBetween(Integer value1, Integer value2) {
            addCriterion("applyRecordId not between", value1, value2, "applyRecordId");
            return (Criteria) this;
        }

        public Criteria andHandlerIdIsNull() {
            addCriterion("handlerId is null");
            return (Criteria) this;
        }

        public Criteria andHandlerIdIsNotNull() {
            addCriterion("handlerId is not null");
            return (Criteria) this;
        }

        public Criteria andHandlerIdEqualTo(Integer value) {
            addCriterion("handlerId =", value, "handlerId");
            return (Criteria) this;
        }

        public Criteria andHandlerIdNotEqualTo(Integer value) {
            addCriterion("handlerId <>", value, "handlerId");
            return (Criteria) this;
        }

        public Criteria andHandlerIdGreaterThan(Integer value) {
            addCriterion("handlerId >", value, "handlerId");
            return (Criteria) this;
        }

        public Criteria andHandlerIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("handlerId >=", value, "handlerId");
            return (Criteria) this;
        }

        public Criteria andHandlerIdLessThan(Integer value) {
            addCriterion("handlerId <", value, "handlerId");
            return (Criteria) this;
        }

        public Criteria andHandlerIdLessThanOrEqualTo(Integer value) {
            addCriterion("handlerId <=", value, "handlerId");
            return (Criteria) this;
        }

        public Criteria andHandlerIdIn(List<Integer> values) {
            addCriterion("handlerId in", values, "handlerId");
            return (Criteria) this;
        }

        public Criteria andHandlerIdNotIn(List<Integer> values) {
            addCriterion("handlerId not in", values, "handlerId");
            return (Criteria) this;
        }

        public Criteria andHandlerIdBetween(Integer value1, Integer value2) {
            addCriterion("handlerId between", value1, value2, "handlerId");
            return (Criteria) this;
        }

        public Criteria andHandlerIdNotBetween(Integer value1, Integer value2) {
            addCriterion("handlerId not between", value1, value2, "handlerId");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeIsNull() {
            addCriterion("createdTime is null");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeIsNotNull() {
            addCriterion("createdTime is not null");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeEqualTo(Date value) {
            addCriterion("createdTime =", value, "createdTime");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeNotEqualTo(Date value) {
            addCriterion("createdTime <>", value, "createdTime");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeGreaterThan(Date value) {
            addCriterion("createdTime >", value, "createdTime");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("createdTime >=", value, "createdTime");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeLessThan(Date value) {
            addCriterion("createdTime <", value, "createdTime");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeLessThanOrEqualTo(Date value) {
            addCriterion("createdTime <=", value, "createdTime");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeIn(List<Date> values) {
            addCriterion("createdTime in", values, "createdTime");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeNotIn(List<Date> values) {
            addCriterion("createdTime not in", values, "createdTime");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeBetween(Date value1, Date value2) {
            addCriterion("createdTime between", value1, value2, "createdTime");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeNotBetween(Date value1, Date value2) {
            addCriterion("createdTime not between", value1, value2, "createdTime");
            return (Criteria) this;
        }

        public Criteria andHandleTimeIsNull() {
            addCriterion("handleTime is null");
            return (Criteria) this;
        }

        public Criteria andHandleTimeIsNotNull() {
            addCriterion("handleTime is not null");
            return (Criteria) this;
        }

        public Criteria andHandleTimeEqualTo(Date value) {
            addCriterion("handleTime =", value, "handleTime");
            return (Criteria) this;
        }

        public Criteria andHandleTimeNotEqualTo(Date value) {
            addCriterion("handleTime <>", value, "handleTime");
            return (Criteria) this;
        }

        public Criteria andHandleTimeGreaterThan(Date value) {
            addCriterion("handleTime >", value, "handleTime");
            return (Criteria) this;
        }

        public Criteria andHandleTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("handleTime >=", value, "handleTime");
            return (Criteria) this;
        }

        public Criteria andHandleTimeLessThan(Date value) {
            addCriterion("handleTime <", value, "handleTime");
            return (Criteria) this;
        }

        public Criteria andHandleTimeLessThanOrEqualTo(Date value) {
            addCriterion("handleTime <=", value, "handleTime");
            return (Criteria) this;
        }

        public Criteria andHandleTimeIn(List<Date> values) {
            addCriterion("handleTime in", values, "handleTime");
            return (Criteria) this;
        }

        public Criteria andHandleTimeNotIn(List<Date> values) {
            addCriterion("handleTime not in", values, "handleTime");
            return (Criteria) this;
        }

        public Criteria andHandleTimeBetween(Date value1, Date value2) {
            addCriterion("handleTime between", value1, value2, "handleTime");
            return (Criteria) this;
        }

        public Criteria andHandleTimeNotBetween(Date value1, Date value2) {
            addCriterion("handleTime not between", value1, value2, "handleTime");
            return (Criteria) this;
        }

        public Criteria andUpdatedTimeIsNull() {
            addCriterion("updatedTime is null");
            return (Criteria) this;
        }

        public Criteria andUpdatedTimeIsNotNull() {
            addCriterion("updatedTime is not null");
            return (Criteria) this;
        }

        public Criteria andUpdatedTimeEqualTo(Date value) {
            addCriterion("updatedTime =", value, "updatedTime");
            return (Criteria) this;
        }

        public Criteria andUpdatedTimeNotEqualTo(Date value) {
            addCriterion("updatedTime <>", value, "updatedTime");
            return (Criteria) this;
        }

        public Criteria andUpdatedTimeGreaterThan(Date value) {
            addCriterion("updatedTime >", value, "updatedTime");
            return (Criteria) this;
        }

        public Criteria andUpdatedTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("updatedTime >=", value, "updatedTime");
            return (Criteria) this;
        }

        public Criteria andUpdatedTimeLessThan(Date value) {
            addCriterion("updatedTime <", value, "updatedTime");
            return (Criteria) this;
        }

        public Criteria andUpdatedTimeLessThanOrEqualTo(Date value) {
            addCriterion("updatedTime <=", value, "updatedTime");
            return (Criteria) this;
        }

        public Criteria andUpdatedTimeIn(List<Date> values) {
            addCriterion("updatedTime in", values, "updatedTime");
            return (Criteria) this;
        }

        public Criteria andUpdatedTimeNotIn(List<Date> values) {
            addCriterion("updatedTime not in", values, "updatedTime");
            return (Criteria) this;
        }

        public Criteria andUpdatedTimeBetween(Date value1, Date value2) {
            addCriterion("updatedTime between", value1, value2, "updatedTime");
            return (Criteria) this;
        }

        public Criteria andUpdatedTimeNotBetween(Date value1, Date value2) {
            addCriterion("updatedTime not between", value1, value2, "updatedTime");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}