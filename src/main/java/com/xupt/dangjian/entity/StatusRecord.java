package com.xupt.dangjian.entity;

import java.util.Date;

public class StatusRecord {
    private Integer id;

    private Integer statusId;

    private Integer preRecordId;

    private Integer nextRecordId;

    private Integer applyRecordId;

    private Integer handlerId;

    private Date createdTime;

    private Date handleTime;

    private Date updatedTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getStatusId() {
        return statusId;
    }

    public void setStatusId(Integer statusId) {
        this.statusId = statusId;
    }

    public Integer getPreRecordId() {
        return preRecordId;
    }

    public void setPreRecordId(Integer preRecordId) {
        this.preRecordId = preRecordId;
    }

    public Integer getNextRecordId() {
        return nextRecordId;
    }

    public void setNextRecordId(Integer nextRecordId) {
        this.nextRecordId = nextRecordId;
    }

    public Integer getApplyRecordId() {
        return applyRecordId;
    }

    public void setApplyRecordId(Integer applyRecordId) {
        this.applyRecordId = applyRecordId;
    }

    public Integer getHandlerId() {
        return handlerId;
    }

    public void setHandlerId(Integer handlerId) {
        this.handlerId = handlerId;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    public Date getHandleTime() {
        return handleTime;
    }

    public void setHandleTime(Date handleTime) {
        this.handleTime = handleTime;
    }

    public Date getUpdatedTime() {
        return updatedTime;
    }

    public void setUpdatedTime(Date updatedTime) {
        this.updatedTime = updatedTime;
    }
}