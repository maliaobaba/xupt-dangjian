package com.xupt.dangjian.entity;

import java.util.Date;

public class ApplyRecord {
    private Integer id;

    private Integer applyId;

    private String content;

    private Integer applyUserId;

    private Date createdTime;

    private Date updatedTime;

    private Integer currentStatusRecordId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getApplyId() {
        return applyId;
    }

    public void setApplyId(Integer applyId) {
        this.applyId = applyId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }

    public Integer getApplyUserId() {
        return applyUserId;
    }

    public void setApplyUserId(Integer applyUserId) {
        this.applyUserId = applyUserId;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    public Date getUpdatedTime() {
        return updatedTime;
    }

    public void setUpdatedTime(Date updatedTime) {
        this.updatedTime = updatedTime;
    }

    public Integer getCurrentStatusRecordId() {
        return currentStatusRecordId;
    }

    public void setCurrentStatusRecordId(Integer currentStatusRecordId) {
        this.currentStatusRecordId = currentStatusRecordId;
    }
}