package com.xupt.dangjian.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ApplyStatusExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public ApplyStatusExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andApplyIdIsNull() {
            addCriterion("applyId is null");
            return (Criteria) this;
        }

        public Criteria andApplyIdIsNotNull() {
            addCriterion("applyId is not null");
            return (Criteria) this;
        }

        public Criteria andApplyIdEqualTo(Integer value) {
            addCriterion("applyId =", value, "applyId");
            return (Criteria) this;
        }

        public Criteria andApplyIdNotEqualTo(Integer value) {
            addCriterion("applyId <>", value, "applyId");
            return (Criteria) this;
        }

        public Criteria andApplyIdGreaterThan(Integer value) {
            addCriterion("applyId >", value, "applyId");
            return (Criteria) this;
        }

        public Criteria andApplyIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("applyId >=", value, "applyId");
            return (Criteria) this;
        }

        public Criteria andApplyIdLessThan(Integer value) {
            addCriterion("applyId <", value, "applyId");
            return (Criteria) this;
        }

        public Criteria andApplyIdLessThanOrEqualTo(Integer value) {
            addCriterion("applyId <=", value, "applyId");
            return (Criteria) this;
        }

        public Criteria andApplyIdIn(List<Integer> values) {
            addCriterion("applyId in", values, "applyId");
            return (Criteria) this;
        }

        public Criteria andApplyIdNotIn(List<Integer> values) {
            addCriterion("applyId not in", values, "applyId");
            return (Criteria) this;
        }

        public Criteria andApplyIdBetween(Integer value1, Integer value2) {
            addCriterion("applyId between", value1, value2, "applyId");
            return (Criteria) this;
        }

        public Criteria andApplyIdNotBetween(Integer value1, Integer value2) {
            addCriterion("applyId not between", value1, value2, "applyId");
            return (Criteria) this;
        }

        public Criteria andStatusNameIsNull() {
            addCriterion("statusName is null");
            return (Criteria) this;
        }

        public Criteria andStatusNameIsNotNull() {
            addCriterion("statusName is not null");
            return (Criteria) this;
        }

        public Criteria andStatusNameEqualTo(String value) {
            addCriterion("statusName =", value, "statusName");
            return (Criteria) this;
        }

        public Criteria andStatusNameNotEqualTo(String value) {
            addCriterion("statusName <>", value, "statusName");
            return (Criteria) this;
        }

        public Criteria andStatusNameGreaterThan(String value) {
            addCriterion("statusName >", value, "statusName");
            return (Criteria) this;
        }

        public Criteria andStatusNameGreaterThanOrEqualTo(String value) {
            addCriterion("statusName >=", value, "statusName");
            return (Criteria) this;
        }

        public Criteria andStatusNameLessThan(String value) {
            addCriterion("statusName <", value, "statusName");
            return (Criteria) this;
        }

        public Criteria andStatusNameLessThanOrEqualTo(String value) {
            addCriterion("statusName <=", value, "statusName");
            return (Criteria) this;
        }

        public Criteria andStatusNameLike(String value) {
            addCriterion("statusName like", value, "statusName");
            return (Criteria) this;
        }

        public Criteria andStatusNameNotLike(String value) {
            addCriterion("statusName not like", value, "statusName");
            return (Criteria) this;
        }

        public Criteria andStatusNameIn(List<String> values) {
            addCriterion("statusName in", values, "statusName");
            return (Criteria) this;
        }

        public Criteria andStatusNameNotIn(List<String> values) {
            addCriterion("statusName not in", values, "statusName");
            return (Criteria) this;
        }

        public Criteria andStatusNameBetween(String value1, String value2) {
            addCriterion("statusName between", value1, value2, "statusName");
            return (Criteria) this;
        }

        public Criteria andStatusNameNotBetween(String value1, String value2) {
            addCriterion("statusName not between", value1, value2, "statusName");
            return (Criteria) this;
        }

        public Criteria andHandleRoleIdIsNull() {
            addCriterion("handleRoleId is null");
            return (Criteria) this;
        }

        public Criteria andHandleRoleIdIsNotNull() {
            addCriterion("handleRoleId is not null");
            return (Criteria) this;
        }

        public Criteria andHandleRoleIdEqualTo(Integer value) {
            addCriterion("handleRoleId =", value, "handleRoleId");
            return (Criteria) this;
        }

        public Criteria andHandleRoleIdNotEqualTo(Integer value) {
            addCriterion("handleRoleId <>", value, "handleRoleId");
            return (Criteria) this;
        }

        public Criteria andHandleRoleIdGreaterThan(Integer value) {
            addCriterion("handleRoleId >", value, "handleRoleId");
            return (Criteria) this;
        }

        public Criteria andHandleRoleIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("handleRoleId >=", value, "handleRoleId");
            return (Criteria) this;
        }

        public Criteria andHandleRoleIdLessThan(Integer value) {
            addCriterion("handleRoleId <", value, "handleRoleId");
            return (Criteria) this;
        }

        public Criteria andHandleRoleIdLessThanOrEqualTo(Integer value) {
            addCriterion("handleRoleId <=", value, "handleRoleId");
            return (Criteria) this;
        }

        public Criteria andHandleRoleIdIn(List<Integer> values) {
            addCriterion("handleRoleId in", values, "handleRoleId");
            return (Criteria) this;
        }

        public Criteria andHandleRoleIdNotIn(List<Integer> values) {
            addCriterion("handleRoleId not in", values, "handleRoleId");
            return (Criteria) this;
        }

        public Criteria andHandleRoleIdBetween(Integer value1, Integer value2) {
            addCriterion("handleRoleId between", value1, value2, "handleRoleId");
            return (Criteria) this;
        }

        public Criteria andHandleRoleIdNotBetween(Integer value1, Integer value2) {
            addCriterion("handleRoleId not between", value1, value2, "handleRoleId");
            return (Criteria) this;
        }

        public Criteria andIsStartStatusIsNull() {
            addCriterion("isStartStatus is null");
            return (Criteria) this;
        }

        public Criteria andIsStartStatusIsNotNull() {
            addCriterion("isStartStatus is not null");
            return (Criteria) this;
        }

        public Criteria andIsStartStatusEqualTo(Integer value) {
            addCriterion("isStartStatus =", value, "isStartStatus");
            return (Criteria) this;
        }

        public Criteria andIsStartStatusNotEqualTo(Integer value) {
            addCriterion("isStartStatus <>", value, "isStartStatus");
            return (Criteria) this;
        }

        public Criteria andIsStartStatusGreaterThan(Integer value) {
            addCriterion("isStartStatus >", value, "isStartStatus");
            return (Criteria) this;
        }

        public Criteria andIsStartStatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("isStartStatus >=", value, "isStartStatus");
            return (Criteria) this;
        }

        public Criteria andIsStartStatusLessThan(Integer value) {
            addCriterion("isStartStatus <", value, "isStartStatus");
            return (Criteria) this;
        }

        public Criteria andIsStartStatusLessThanOrEqualTo(Integer value) {
            addCriterion("isStartStatus <=", value, "isStartStatus");
            return (Criteria) this;
        }

        public Criteria andIsStartStatusIn(List<Integer> values) {
            addCriterion("isStartStatus in", values, "isStartStatus");
            return (Criteria) this;
        }

        public Criteria andIsStartStatusNotIn(List<Integer> values) {
            addCriterion("isStartStatus not in", values, "isStartStatus");
            return (Criteria) this;
        }

        public Criteria andIsStartStatusBetween(Integer value1, Integer value2) {
            addCriterion("isStartStatus between", value1, value2, "isStartStatus");
            return (Criteria) this;
        }

        public Criteria andIsStartStatusNotBetween(Integer value1, Integer value2) {
            addCriterion("isStartStatus not between", value1, value2, "isStartStatus");
            return (Criteria) this;
        }

        public Criteria andIsFinalStatusIsNull() {
            addCriterion("isFinalStatus is null");
            return (Criteria) this;
        }

        public Criteria andIsFinalStatusIsNotNull() {
            addCriterion("isFinalStatus is not null");
            return (Criteria) this;
        }

        public Criteria andIsFinalStatusEqualTo(Integer value) {
            addCriterion("isFinalStatus =", value, "isFinalStatus");
            return (Criteria) this;
        }

        public Criteria andIsFinalStatusNotEqualTo(Integer value) {
            addCriterion("isFinalStatus <>", value, "isFinalStatus");
            return (Criteria) this;
        }

        public Criteria andIsFinalStatusGreaterThan(Integer value) {
            addCriterion("isFinalStatus >", value, "isFinalStatus");
            return (Criteria) this;
        }

        public Criteria andIsFinalStatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("isFinalStatus >=", value, "isFinalStatus");
            return (Criteria) this;
        }

        public Criteria andIsFinalStatusLessThan(Integer value) {
            addCriterion("isFinalStatus <", value, "isFinalStatus");
            return (Criteria) this;
        }

        public Criteria andIsFinalStatusLessThanOrEqualTo(Integer value) {
            addCriterion("isFinalStatus <=", value, "isFinalStatus");
            return (Criteria) this;
        }

        public Criteria andIsFinalStatusIn(List<Integer> values) {
            addCriterion("isFinalStatus in", values, "isFinalStatus");
            return (Criteria) this;
        }

        public Criteria andIsFinalStatusNotIn(List<Integer> values) {
            addCriterion("isFinalStatus not in", values, "isFinalStatus");
            return (Criteria) this;
        }

        public Criteria andIsFinalStatusBetween(Integer value1, Integer value2) {
            addCriterion("isFinalStatus between", value1, value2, "isFinalStatus");
            return (Criteria) this;
        }

        public Criteria andIsFinalStatusNotBetween(Integer value1, Integer value2) {
            addCriterion("isFinalStatus not between", value1, value2, "isFinalStatus");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeIsNull() {
            addCriterion("createdTime is null");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeIsNotNull() {
            addCriterion("createdTime is not null");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeEqualTo(Date value) {
            addCriterion("createdTime =", value, "createdTime");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeNotEqualTo(Date value) {
            addCriterion("createdTime <>", value, "createdTime");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeGreaterThan(Date value) {
            addCriterion("createdTime >", value, "createdTime");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("createdTime >=", value, "createdTime");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeLessThan(Date value) {
            addCriterion("createdTime <", value, "createdTime");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeLessThanOrEqualTo(Date value) {
            addCriterion("createdTime <=", value, "createdTime");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeIn(List<Date> values) {
            addCriterion("createdTime in", values, "createdTime");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeNotIn(List<Date> values) {
            addCriterion("createdTime not in", values, "createdTime");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeBetween(Date value1, Date value2) {
            addCriterion("createdTime between", value1, value2, "createdTime");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeNotBetween(Date value1, Date value2) {
            addCriterion("createdTime not between", value1, value2, "createdTime");
            return (Criteria) this;
        }

        public Criteria andUpdatedTimeIsNull() {
            addCriterion("updatedTime is null");
            return (Criteria) this;
        }

        public Criteria andUpdatedTimeIsNotNull() {
            addCriterion("updatedTime is not null");
            return (Criteria) this;
        }

        public Criteria andUpdatedTimeEqualTo(Date value) {
            addCriterion("updatedTime =", value, "updatedTime");
            return (Criteria) this;
        }

        public Criteria andUpdatedTimeNotEqualTo(Date value) {
            addCriterion("updatedTime <>", value, "updatedTime");
            return (Criteria) this;
        }

        public Criteria andUpdatedTimeGreaterThan(Date value) {
            addCriterion("updatedTime >", value, "updatedTime");
            return (Criteria) this;
        }

        public Criteria andUpdatedTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("updatedTime >=", value, "updatedTime");
            return (Criteria) this;
        }

        public Criteria andUpdatedTimeLessThan(Date value) {
            addCriterion("updatedTime <", value, "updatedTime");
            return (Criteria) this;
        }

        public Criteria andUpdatedTimeLessThanOrEqualTo(Date value) {
            addCriterion("updatedTime <=", value, "updatedTime");
            return (Criteria) this;
        }

        public Criteria andUpdatedTimeIn(List<Date> values) {
            addCriterion("updatedTime in", values, "updatedTime");
            return (Criteria) this;
        }

        public Criteria andUpdatedTimeNotIn(List<Date> values) {
            addCriterion("updatedTime not in", values, "updatedTime");
            return (Criteria) this;
        }

        public Criteria andUpdatedTimeBetween(Date value1, Date value2) {
            addCriterion("updatedTime between", value1, value2, "updatedTime");
            return (Criteria) this;
        }

        public Criteria andUpdatedTimeNotBetween(Date value1, Date value2) {
            addCriterion("updatedTime not between", value1, value2, "updatedTime");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}