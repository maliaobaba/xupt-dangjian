package com.xupt.dangjian.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ArticleExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public ArticleExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andTitleIsNull() {
            addCriterion("title is null");
            return (Criteria) this;
        }

        public Criteria andTitleIsNotNull() {
            addCriterion("title is not null");
            return (Criteria) this;
        }

        public Criteria andTitleEqualTo(String value) {
            addCriterion("title =", value, "title");
            return (Criteria) this;
        }

        public Criteria andTitleNotEqualTo(String value) {
            addCriterion("title <>", value, "title");
            return (Criteria) this;
        }

        public Criteria andTitleGreaterThan(String value) {
            addCriterion("title >", value, "title");
            return (Criteria) this;
        }

        public Criteria andTitleGreaterThanOrEqualTo(String value) {
            addCriterion("title >=", value, "title");
            return (Criteria) this;
        }

        public Criteria andTitleLessThan(String value) {
            addCriterion("title <", value, "title");
            return (Criteria) this;
        }

        public Criteria andTitleLessThanOrEqualTo(String value) {
            addCriterion("title <=", value, "title");
            return (Criteria) this;
        }

        public Criteria andTitleLike(String value) {
            addCriterion("title like", value, "title");
            return (Criteria) this;
        }

        public Criteria andTitleNotLike(String value) {
            addCriterion("title not like", value, "title");
            return (Criteria) this;
        }

        public Criteria andTitleIn(List<String> values) {
            addCriterion("title in", values, "title");
            return (Criteria) this;
        }

        public Criteria andTitleNotIn(List<String> values) {
            addCriterion("title not in", values, "title");
            return (Criteria) this;
        }

        public Criteria andTitleBetween(String value1, String value2) {
            addCriterion("title between", value1, value2, "title");
            return (Criteria) this;
        }

        public Criteria andTitleNotBetween(String value1, String value2) {
            addCriterion("title not between", value1, value2, "title");
            return (Criteria) this;
        }

        public Criteria andAuthorIsNull() {
            addCriterion("author is null");
            return (Criteria) this;
        }

        public Criteria andAuthorIsNotNull() {
            addCriterion("author is not null");
            return (Criteria) this;
        }

        public Criteria andAuthorEqualTo(String value) {
            addCriterion("author =", value, "author");
            return (Criteria) this;
        }

        public Criteria andAuthorNotEqualTo(String value) {
            addCriterion("author <>", value, "author");
            return (Criteria) this;
        }

        public Criteria andAuthorGreaterThan(String value) {
            addCriterion("author >", value, "author");
            return (Criteria) this;
        }

        public Criteria andAuthorGreaterThanOrEqualTo(String value) {
            addCriterion("author >=", value, "author");
            return (Criteria) this;
        }

        public Criteria andAuthorLessThan(String value) {
            addCriterion("author <", value, "author");
            return (Criteria) this;
        }

        public Criteria andAuthorLessThanOrEqualTo(String value) {
            addCriterion("author <=", value, "author");
            return (Criteria) this;
        }

        public Criteria andAuthorLike(String value) {
            addCriterion("author like", value, "author");
            return (Criteria) this;
        }

        public Criteria andAuthorNotLike(String value) {
            addCriterion("author not like", value, "author");
            return (Criteria) this;
        }

        public Criteria andAuthorIn(List<String> values) {
            addCriterion("author in", values, "author");
            return (Criteria) this;
        }

        public Criteria andAuthorNotIn(List<String> values) {
            addCriterion("author not in", values, "author");
            return (Criteria) this;
        }

        public Criteria andAuthorBetween(String value1, String value2) {
            addCriterion("author between", value1, value2, "author");
            return (Criteria) this;
        }

        public Criteria andAuthorNotBetween(String value1, String value2) {
            addCriterion("author not between", value1, value2, "author");
            return (Criteria) this;
        }

        public Criteria andViewTimesIsNull() {
            addCriterion("viewTimes is null");
            return (Criteria) this;
        }

        public Criteria andViewTimesIsNotNull() {
            addCriterion("viewTimes is not null");
            return (Criteria) this;
        }

        public Criteria andViewTimesEqualTo(Integer value) {
            addCriterion("viewTimes =", value, "viewTimes");
            return (Criteria) this;
        }

        public Criteria andViewTimesNotEqualTo(Integer value) {
            addCriterion("viewTimes <>", value, "viewTimes");
            return (Criteria) this;
        }

        public Criteria andViewTimesGreaterThan(Integer value) {
            addCriterion("viewTimes >", value, "viewTimes");
            return (Criteria) this;
        }

        public Criteria andViewTimesGreaterThanOrEqualTo(Integer value) {
            addCriterion("viewTimes >=", value, "viewTimes");
            return (Criteria) this;
        }

        public Criteria andViewTimesLessThan(Integer value) {
            addCriterion("viewTimes <", value, "viewTimes");
            return (Criteria) this;
        }

        public Criteria andViewTimesLessThanOrEqualTo(Integer value) {
            addCriterion("viewTimes <=", value, "viewTimes");
            return (Criteria) this;
        }

        public Criteria andViewTimesIn(List<Integer> values) {
            addCriterion("viewTimes in", values, "viewTimes");
            return (Criteria) this;
        }

        public Criteria andViewTimesNotIn(List<Integer> values) {
            addCriterion("viewTimes not in", values, "viewTimes");
            return (Criteria) this;
        }

        public Criteria andViewTimesBetween(Integer value1, Integer value2) {
            addCriterion("viewTimes between", value1, value2, "viewTimes");
            return (Criteria) this;
        }

        public Criteria andViewTimesNotBetween(Integer value1, Integer value2) {
            addCriterion("viewTimes not between", value1, value2, "viewTimes");
            return (Criteria) this;
        }

        public Criteria andArticleClassIsNull() {
            addCriterion("articleClass is null");
            return (Criteria) this;
        }

        public Criteria andArticleClassIsNotNull() {
            addCriterion("articleClass is not null");
            return (Criteria) this;
        }

        public Criteria andArticleClassEqualTo(String value) {
            addCriterion("articleClass =", value, "articleClass");
            return (Criteria) this;
        }

        public Criteria andArticleClassNotEqualTo(String value) {
            addCriterion("articleClass <>", value, "articleClass");
            return (Criteria) this;
        }

        public Criteria andArticleClassGreaterThan(String value) {
            addCriterion("articleClass >", value, "articleClass");
            return (Criteria) this;
        }

        public Criteria andArticleClassGreaterThanOrEqualTo(String value) {
            addCriterion("articleClass >=", value, "articleClass");
            return (Criteria) this;
        }

        public Criteria andArticleClassLessThan(String value) {
            addCriterion("articleClass <", value, "articleClass");
            return (Criteria) this;
        }

        public Criteria andArticleClassLessThanOrEqualTo(String value) {
            addCriterion("articleClass <=", value, "articleClass");
            return (Criteria) this;
        }

        public Criteria andArticleClassLike(String value) {
            addCriterion("articleClass like", value, "articleClass");
            return (Criteria) this;
        }

        public Criteria andArticleClassNotLike(String value) {
            addCriterion("articleClass not like", value, "articleClass");
            return (Criteria) this;
        }

        public Criteria andArticleClassIn(List<String> values) {
            addCriterion("articleClass in", values, "articleClass");
            return (Criteria) this;
        }

        public Criteria andArticleClassNotIn(List<String> values) {
            addCriterion("articleClass not in", values, "articleClass");
            return (Criteria) this;
        }

        public Criteria andArticleClassBetween(String value1, String value2) {
            addCriterion("articleClass between", value1, value2, "articleClass");
            return (Criteria) this;
        }

        public Criteria andArticleClassNotBetween(String value1, String value2) {
            addCriterion("articleClass not between", value1, value2, "articleClass");
            return (Criteria) this;
        }

        public Criteria andDangzhanIdIsNull() {
            addCriterion("dangzhanId is null");
            return (Criteria) this;
        }

        public Criteria andDangzhanIdIsNotNull() {
            addCriterion("dangzhanId is not null");
            return (Criteria) this;
        }

        public Criteria andDangzhanIdEqualTo(Integer value) {
            addCriterion("dangzhanId =", value, "dangzhanId");
            return (Criteria) this;
        }

        public Criteria andDangzhanIdNotEqualTo(Integer value) {
            addCriterion("dangzhanId <>", value, "dangzhanId");
            return (Criteria) this;
        }

        public Criteria andDangzhanIdGreaterThan(Integer value) {
            addCriterion("dangzhanId >", value, "dangzhanId");
            return (Criteria) this;
        }

        public Criteria andDangzhanIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("dangzhanId >=", value, "dangzhanId");
            return (Criteria) this;
        }

        public Criteria andDangzhanIdLessThan(Integer value) {
            addCriterion("dangzhanId <", value, "dangzhanId");
            return (Criteria) this;
        }

        public Criteria andDangzhanIdLessThanOrEqualTo(Integer value) {
            addCriterion("dangzhanId <=", value, "dangzhanId");
            return (Criteria) this;
        }

        public Criteria andDangzhanIdIn(List<Integer> values) {
            addCriterion("dangzhanId in", values, "dangzhanId");
            return (Criteria) this;
        }

        public Criteria andDangzhanIdNotIn(List<Integer> values) {
            addCriterion("dangzhanId not in", values, "dangzhanId");
            return (Criteria) this;
        }

        public Criteria andDangzhanIdBetween(Integer value1, Integer value2) {
            addCriterion("dangzhanId between", value1, value2, "dangzhanId");
            return (Criteria) this;
        }

        public Criteria andDangzhanIdNotBetween(Integer value1, Integer value2) {
            addCriterion("dangzhanId not between", value1, value2, "dangzhanId");
            return (Criteria) this;
        }

        public Criteria andContentIsNull() {
            addCriterion("content is null");
            return (Criteria) this;
        }

        public Criteria andContentIsNotNull() {
            addCriterion("content is not null");
            return (Criteria) this;
        }

        public Criteria andContentEqualTo(String value) {
            addCriterion("content =", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentNotEqualTo(String value) {
            addCriterion("content <>", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentGreaterThan(String value) {
            addCriterion("content >", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentGreaterThanOrEqualTo(String value) {
            addCriterion("content >=", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentLessThan(String value) {
            addCriterion("content <", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentLessThanOrEqualTo(String value) {
            addCriterion("content <=", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentLike(String value) {
            addCriterion("content like", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentNotLike(String value) {
            addCriterion("content not like", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentIn(List<String> values) {
            addCriterion("content in", values, "content");
            return (Criteria) this;
        }

        public Criteria andContentNotIn(List<String> values) {
            addCriterion("content not in", values, "content");
            return (Criteria) this;
        }

        public Criteria andContentBetween(String value1, String value2) {
            addCriterion("content between", value1, value2, "content");
            return (Criteria) this;
        }

        public Criteria andContentNotBetween(String value1, String value2) {
            addCriterion("content not between", value1, value2, "content");
            return (Criteria) this;
        }

        public Criteria andCoverImageIdIsNull() {
            addCriterion("coverImageId is null");
            return (Criteria) this;
        }

        public Criteria andCoverImageIdIsNotNull() {
            addCriterion("coverImageId is not null");
            return (Criteria) this;
        }

        public Criteria andCoverImageIdEqualTo(Integer value) {
            addCriterion("coverImageId =", value, "coverImageId");
            return (Criteria) this;
        }

        public Criteria andCoverImageIdNotEqualTo(Integer value) {
            addCriterion("coverImageId <>", value, "coverImageId");
            return (Criteria) this;
        }

        public Criteria andCoverImageIdGreaterThan(Integer value) {
            addCriterion("coverImageId >", value, "coverImageId");
            return (Criteria) this;
        }

        public Criteria andCoverImageIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("coverImageId >=", value, "coverImageId");
            return (Criteria) this;
        }

        public Criteria andCoverImageIdLessThan(Integer value) {
            addCriterion("coverImageId <", value, "coverImageId");
            return (Criteria) this;
        }

        public Criteria andCoverImageIdLessThanOrEqualTo(Integer value) {
            addCriterion("coverImageId <=", value, "coverImageId");
            return (Criteria) this;
        }

        public Criteria andCoverImageIdIn(List<Integer> values) {
            addCriterion("coverImageId in", values, "coverImageId");
            return (Criteria) this;
        }

        public Criteria andCoverImageIdNotIn(List<Integer> values) {
            addCriterion("coverImageId not in", values, "coverImageId");
            return (Criteria) this;
        }

        public Criteria andCoverImageIdBetween(Integer value1, Integer value2) {
            addCriterion("coverImageId between", value1, value2, "coverImageId");
            return (Criteria) this;
        }

        public Criteria andCoverImageIdNotBetween(Integer value1, Integer value2) {
            addCriterion("coverImageId not between", value1, value2, "coverImageId");
            return (Criteria) this;
        }

        public Criteria andPublisherIdIsNull() {
            addCriterion("publisherId is null");
            return (Criteria) this;
        }

        public Criteria andPublisherIdIsNotNull() {
            addCriterion("publisherId is not null");
            return (Criteria) this;
        }

        public Criteria andPublisherIdEqualTo(Integer value) {
            addCriterion("publisherId =", value, "publisherId");
            return (Criteria) this;
        }

        public Criteria andPublisherIdNotEqualTo(Integer value) {
            addCriterion("publisherId <>", value, "publisherId");
            return (Criteria) this;
        }

        public Criteria andPublisherIdGreaterThan(Integer value) {
            addCriterion("publisherId >", value, "publisherId");
            return (Criteria) this;
        }

        public Criteria andPublisherIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("publisherId >=", value, "publisherId");
            return (Criteria) this;
        }

        public Criteria andPublisherIdLessThan(Integer value) {
            addCriterion("publisherId <", value, "publisherId");
            return (Criteria) this;
        }

        public Criteria andPublisherIdLessThanOrEqualTo(Integer value) {
            addCriterion("publisherId <=", value, "publisherId");
            return (Criteria) this;
        }

        public Criteria andPublisherIdIn(List<Integer> values) {
            addCriterion("publisherId in", values, "publisherId");
            return (Criteria) this;
        }

        public Criteria andPublisherIdNotIn(List<Integer> values) {
            addCriterion("publisherId not in", values, "publisherId");
            return (Criteria) this;
        }

        public Criteria andPublisherIdBetween(Integer value1, Integer value2) {
            addCriterion("publisherId between", value1, value2, "publisherId");
            return (Criteria) this;
        }

        public Criteria andPublisherIdNotBetween(Integer value1, Integer value2) {
            addCriterion("publisherId not between", value1, value2, "publisherId");
            return (Criteria) this;
        }

        public Criteria andBreviaryIsNull() {
            addCriterion("breviary is null");
            return (Criteria) this;
        }

        public Criteria andBreviaryIsNotNull() {
            addCriterion("breviary is not null");
            return (Criteria) this;
        }

        public Criteria andBreviaryEqualTo(String value) {
            addCriterion("breviary =", value, "breviary");
            return (Criteria) this;
        }

        public Criteria andBreviaryNotEqualTo(String value) {
            addCriterion("breviary <>", value, "breviary");
            return (Criteria) this;
        }

        public Criteria andBreviaryGreaterThan(String value) {
            addCriterion("breviary >", value, "breviary");
            return (Criteria) this;
        }

        public Criteria andBreviaryGreaterThanOrEqualTo(String value) {
            addCriterion("breviary >=", value, "breviary");
            return (Criteria) this;
        }

        public Criteria andBreviaryLessThan(String value) {
            addCriterion("breviary <", value, "breviary");
            return (Criteria) this;
        }

        public Criteria andBreviaryLessThanOrEqualTo(String value) {
            addCriterion("breviary <=", value, "breviary");
            return (Criteria) this;
        }

        public Criteria andBreviaryLike(String value) {
            addCriterion("breviary like", value, "breviary");
            return (Criteria) this;
        }

        public Criteria andBreviaryNotLike(String value) {
            addCriterion("breviary not like", value, "breviary");
            return (Criteria) this;
        }

        public Criteria andBreviaryIn(List<String> values) {
            addCriterion("breviary in", values, "breviary");
            return (Criteria) this;
        }

        public Criteria andBreviaryNotIn(List<String> values) {
            addCriterion("breviary not in", values, "breviary");
            return (Criteria) this;
        }

        public Criteria andBreviaryBetween(String value1, String value2) {
            addCriterion("breviary between", value1, value2, "breviary");
            return (Criteria) this;
        }

        public Criteria andBreviaryNotBetween(String value1, String value2) {
            addCriterion("breviary not between", value1, value2, "breviary");
            return (Criteria) this;
        }

        public Criteria andCoverImageUrlIsNull() {
            addCriterion("coverImageUrl is null");
            return (Criteria) this;
        }

        public Criteria andCoverImageUrlIsNotNull() {
            addCriterion("coverImageUrl is not null");
            return (Criteria) this;
        }

        public Criteria andCoverImageUrlEqualTo(String value) {
            addCriterion("coverImageUrl =", value, "coverImageUrl");
            return (Criteria) this;
        }

        public Criteria andCoverImageUrlNotEqualTo(String value) {
            addCriterion("coverImageUrl <>", value, "coverImageUrl");
            return (Criteria) this;
        }

        public Criteria andCoverImageUrlGreaterThan(String value) {
            addCriterion("coverImageUrl >", value, "coverImageUrl");
            return (Criteria) this;
        }

        public Criteria andCoverImageUrlGreaterThanOrEqualTo(String value) {
            addCriterion("coverImageUrl >=", value, "coverImageUrl");
            return (Criteria) this;
        }

        public Criteria andCoverImageUrlLessThan(String value) {
            addCriterion("coverImageUrl <", value, "coverImageUrl");
            return (Criteria) this;
        }

        public Criteria andCoverImageUrlLessThanOrEqualTo(String value) {
            addCriterion("coverImageUrl <=", value, "coverImageUrl");
            return (Criteria) this;
        }

        public Criteria andCoverImageUrlLike(String value) {
            addCriterion("coverImageUrl like", value, "coverImageUrl");
            return (Criteria) this;
        }

        public Criteria andCoverImageUrlNotLike(String value) {
            addCriterion("coverImageUrl not like", value, "coverImageUrl");
            return (Criteria) this;
        }

        public Criteria andCoverImageUrlIn(List<String> values) {
            addCriterion("coverImageUrl in", values, "coverImageUrl");
            return (Criteria) this;
        }

        public Criteria andCoverImageUrlNotIn(List<String> values) {
            addCriterion("coverImageUrl not in", values, "coverImageUrl");
            return (Criteria) this;
        }

        public Criteria andCoverImageUrlBetween(String value1, String value2) {
            addCriterion("coverImageUrl between", value1, value2, "coverImageUrl");
            return (Criteria) this;
        }

        public Criteria andCoverImageUrlNotBetween(String value1, String value2) {
            addCriterion("coverImageUrl not between", value1, value2, "coverImageUrl");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeIsNull() {
            addCriterion("createdTime is null");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeIsNotNull() {
            addCriterion("createdTime is not null");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeEqualTo(Date value) {
            addCriterion("createdTime =", value, "createdTime");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeNotEqualTo(Date value) {
            addCriterion("createdTime <>", value, "createdTime");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeGreaterThan(Date value) {
            addCriterion("createdTime >", value, "createdTime");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("createdTime >=", value, "createdTime");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeLessThan(Date value) {
            addCriterion("createdTime <", value, "createdTime");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeLessThanOrEqualTo(Date value) {
            addCriterion("createdTime <=", value, "createdTime");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeIn(List<Date> values) {
            addCriterion("createdTime in", values, "createdTime");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeNotIn(List<Date> values) {
            addCriterion("createdTime not in", values, "createdTime");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeBetween(Date value1, Date value2) {
            addCriterion("createdTime between", value1, value2, "createdTime");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeNotBetween(Date value1, Date value2) {
            addCriterion("createdTime not between", value1, value2, "createdTime");
            return (Criteria) this;
        }

        public Criteria andUpdatedTimeIsNull() {
            addCriterion("updatedTime is null");
            return (Criteria) this;
        }

        public Criteria andUpdatedTimeIsNotNull() {
            addCriterion("updatedTime is not null");
            return (Criteria) this;
        }

        public Criteria andUpdatedTimeEqualTo(Date value) {
            addCriterion("updatedTime =", value, "updatedTime");
            return (Criteria) this;
        }

        public Criteria andUpdatedTimeNotEqualTo(Date value) {
            addCriterion("updatedTime <>", value, "updatedTime");
            return (Criteria) this;
        }

        public Criteria andUpdatedTimeGreaterThan(Date value) {
            addCriterion("updatedTime >", value, "updatedTime");
            return (Criteria) this;
        }

        public Criteria andUpdatedTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("updatedTime >=", value, "updatedTime");
            return (Criteria) this;
        }

        public Criteria andUpdatedTimeLessThan(Date value) {
            addCriterion("updatedTime <", value, "updatedTime");
            return (Criteria) this;
        }

        public Criteria andUpdatedTimeLessThanOrEqualTo(Date value) {
            addCriterion("updatedTime <=", value, "updatedTime");
            return (Criteria) this;
        }

        public Criteria andUpdatedTimeIn(List<Date> values) {
            addCriterion("updatedTime in", values, "updatedTime");
            return (Criteria) this;
        }

        public Criteria andUpdatedTimeNotIn(List<Date> values) {
            addCriterion("updatedTime not in", values, "updatedTime");
            return (Criteria) this;
        }

        public Criteria andUpdatedTimeBetween(Date value1, Date value2) {
            addCriterion("updatedTime between", value1, value2, "updatedTime");
            return (Criteria) this;
        }

        public Criteria andUpdatedTimeNotBetween(Date value1, Date value2) {
            addCriterion("updatedTime not between", value1, value2, "updatedTime");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}