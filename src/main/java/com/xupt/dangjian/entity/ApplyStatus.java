package com.xupt.dangjian.entity;

import java.util.Date;

public class ApplyStatus {
    private Integer id;

    private Integer applyId;

    private String statusName;

    private Integer handleRoleId;

    private Integer isStartStatus;

    private Integer isFinalStatus;

    private Date createdTime;

    private Date updatedTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getApplyId() {
        return applyId;
    }

    public void setApplyId(Integer applyId) {
        this.applyId = applyId;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName == null ? null : statusName.trim();
    }

    public Integer getHandleRoleId() {
        return handleRoleId;
    }

    public void setHandleRoleId(Integer handleRoleId) {
        this.handleRoleId = handleRoleId;
    }

    public Integer getIsStartStatus() {
        return isStartStatus;
    }

    public void setIsStartStatus(Integer isStartStatus) {
        this.isStartStatus = isStartStatus;
    }

    public Integer getIsFinalStatus() {
        return isFinalStatus;
    }

    public void setIsFinalStatus(Integer isFinalStatus) {
        this.isFinalStatus = isFinalStatus;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    public Date getUpdatedTime() {
        return updatedTime;
    }

    public void setUpdatedTime(Date updatedTime) {
        this.updatedTime = updatedTime;
    }
}